# 2022 N2C2 Track 3: Assessment and Plan Reasoning

This is the repository for sample data used in 2022 National NLP Clinical Challenge (N2C2), Track 3: Progress Note Understanding: Assessment and Plan Reasoning. The annotation on sample data is provided by [ICU Data Science Lab, Department of Medicine, SMPH, UW-Madison](https://www.medicine.wisc.edu/apcc/icu-data-science-lab). We conducted annotations on [MIMIC data](https://physionet.org/content/mimiciii-demo/1.4/), which requires a Data Use Agreement to access the source files. 

### Table of Contents
**[Task](#task)**<br>
**[Example](#example)**<br>
**[Format](#format)**<br>
**[Data Usage](#data)**<br>
**[Authorship](#authorship)**<br>
**[Acknowledgement](#acknowledgement)**<br>
**[Contact](#contact)**<br>

### Task 

We will provide pairs of assessment section and plan subsection (A&P pair). The model should take A&P pairs as input and output one of the four relations: *Direct, Indirect, Neither, Not Relevant*.  The definitions of the four relations are the following: 
```
1) Direct, if the plan subsection targets the primary diagnoses or problem
2) Indirect, if the plan subsection lists problems/diagnoses (e.g. organ failure) associated with the primary diagnosis/problem, including other diagnoses/problems that are not part of the main diagnosis.   
3) Neither, if the plan subsection mentions a problem/diagnosis that is not mentioned in that day’s progress note. 
4) Not Relevant, if the plan subsection does not include a problem/diagnosis
```

### Example 

To explain the task better, we include two examples of A&P pairs in the figure below: 

![Example A&P pair with the annotated relation](ap_samples.png)

In the first example, the plan subsection mentions "Respiratory failure", which is the main diagnosis ("hospital-acquired pneumonia") included in the assessment; therefore, the relation is "Direct".  

In the second example, the plan subsection mentions "Amenia". It is directly *caused* by the "upper GI bleed" mentioned in the assessment section; therefore, the relation between assessment and plan subsection is "Direct". 


### Format
The sample data are provided as csv format with 6 columns: File ID, Assessment Begin, Assessment End, PlanSubsection Begin, PlanSubsection End, Relation. Users can extract the raw text of Assessment and Plan subsection given File ID, and the four character offsets (the begin and end position for Assessment/Plan Subsection). Every row is essentially a training/test sample. Given every pair of Assessment and Plan raw text as input (extracted through every row of records), N2C2 participants should develop model that predicts the Relation. The last column of the csv file provides the ground-truth labels indicating the relations between every pair of assessment and plan subsection (Direct, Indirect, Neither, Not Relevant). 

For example, given a pair of assessment and plan subsection from the file 113085 (file ID in column 1): 

```
File ID,Assessment Begin,Assessment End,PlanSubsection Begin,PlanSubsection End,Relation
113085,4305,4628,4637,5310,Direct
```
The input to relation prediction model has two parts: assessment (with character offset 4305-4628, column 2-3), and plan subsection (with character offset 4637-5310, column 4-5). The output of the model is one of the four relations and could be evaluated against the gold relation (column 6). 


### Data Usage 
Participants must access the MIMIC data through this challenge or PhysioNet, and sign MIMIC's Data Use Agreement. 

### Authorship
This project is joint work by Dr. Yanjun Gao (UW-Madison ICU DataSci Lab), Samuel Tesch (UW-Madison DOM), Ryan Laffin (UW-Madison DOM), Dr. John Caskey (UW-Madison ICU DataSci Lab), Prof. Tim Miller (Boston Children's Hospital, Harvard), Prof. Dimitry Dligach (Loyola), Prof. Matthew Churpek (UW-Madison ICU DataSci Lab) and Prof. Majid Afshar (UW-Madison ICU DataSci Lab). 

### Acknowledgement
We thank Samuel Tesch and Ryan Laffin for their efforts in data annotation. Their work was supported through the Universit of Wisconsin School of Medicine and Public Health's Shapiro Grant. We also thank Prof. Ozlem Uzuner (George Mason) and Harvard Medical School DBMI for hosting the challenge.  

### Contact
Please join our google [discussion group](https://groups.google.com/g/N2C2_UW_ICU_DataSci_2022_AP_Task). For other questions, please contact ygao@medicine.wisc.edu. 
